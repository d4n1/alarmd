#define trigPin 12
#define echoPin 13

const int buzzer = 7;
const int led = 8;
int duration, distance;
                                                                
void setup() {
    Serial.begin(9600);
    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);
    pinMode(buzzer, OUTPUT);
    pinMode(led, OUTPUT);
}

void loop() {
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10000);
    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH);
    distance = (duration / 2) / 29.1;
    
    if (distance >= 200 || distance <= 0) {
        Serial.println("Bot seaching for destroy ...");
        noTone(buzzer);
        digitalWrite(led, LOW);
    } else {
        Serial.println("Bot found something to destroy!");
        Serial.println(distance);
        tone(buzzer, 440);
        digitalWrite(led, HIGH);
    }
}
